import { useNavigate, useParams } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  DeletePostThunkAction,
  UpdatePostThunkAction,
  fetchPostByIdThunkAction,
} from "../redux-toolkit/postSlice";
import { listUserSelector, postSelector } from "../store/selector";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import paths from "../routes";

const schema = yup
  .object({
    id: yup.string(),
    title: yup.string().required(),
    body: yup.string().required(),
    userId: yup.string().required(),
  })
  .required();

function EditPost() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const post = useSelector(postSelector);
  const listUser = useSelector(listUserSelector);
  const navigate = useNavigate();

  const {
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    dispatch(fetchPostByIdThunkAction(id));
  }, [id, dispatch]);

  useEffect(() => {
    setValue("title", post.title);
    setValue("body", post.body);
    setValue("userId", post.userId);
  }, [post.title, post.body, post.userId, setValue]);
  const handleUpdatePost = (values) => {
    dispatch(UpdatePostThunkAction({ ...values, id: id }));
    navigate(paths.home);
  };
  return (
    <MainLayout>
      <div className="container mt-5">
        <h2>Edit Post</h2>
        <form onSubmit={handleSubmit(handleUpdatePost)}>
          <div className="mb-3">
            <label className="form-label">Title</label>
            <input
              type="text"
              className={`${errors?.title?.message ? "is-invalid" : ""} form-control`}
              {...register("title")}
            />
            <span className="invalid-feedback">{errors?.title?.message || ""}</span>
          </div>
          <div className="mb-3">
            <label className="form-label">Author</label>
            <select
              className={`${errors?.userId?.message ? "is-invalid" : ""} form-select`}
              {...register("userId")}
            >
              {listUser?.map((user) => (
                <option key={user?.id} value={user?.id}>
                  {user?.name}
                </option>
              ))}
            </select>
            <span className="invalid-feedback">{errors?.userId?.message}</span>
          </div>
          <div className="mb-3">
            <label className="form-label">Content</label>
            <textarea
              className={`${errors?.body?.message ? "is-invalid" : ""} form-control`}
              {...register("body")}
            />
            <span className="invalid-feedback">{errors?.body?.message || ""}</span>
          </div>
          <button type="submit" className="btn btn-primary w-100 mt-3">
            Save Post
          </button>
        </form>
        <button
          type="button"
          className="btn btn-danger w-100 mt-3"
          onClick={() => {
            dispatch(DeletePostThunkAction(id));
            navigate(paths.home);
          }}
        >
          Delete Post
        </button>
      </div>
    </MainLayout>
  );
}

export default EditPost;
