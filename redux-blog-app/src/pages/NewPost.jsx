import { useNavigate } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";

import { useDispatch, useSelector } from "react-redux";
import { AddPostThunkAction } from "../redux-toolkit/postSlice";
import { listUserSelector } from "../store/selector";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import paths from "../routes";
import { v4 as uuid } from "uuid";

const schema = yup
  .object({
    id: yup.string(),
    title: yup.string().required(),
    body: yup.string().required(),
    userId: yup.string().required(),
  })
  .required();

function NewPost() {
  const dispatch = useDispatch();
  const listUser = useSelector(listUserSelector);
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const handleCreatePost = (values) => {
    dispatch(AddPostThunkAction({ ...values, id: uuid() }));
    navigate(paths.home);
  };
  return (
    <MainLayout>
      <div className="container mt-5">
        <h2>Add Post</h2>
        <form onSubmit={handleSubmit(handleCreatePost)}>
          <div className="mb-3">
            <label className="form-label">Title</label>
            <input
              type="text"
              className={`${errors?.title?.message ? "is-invalid" : ""} form-control`}
              {...register("title")}
            />
            <span className="invalid-feedback">{errors?.title?.message || ""}</span>
          </div>
          <div className="mb-3">
            <label className="form-label">Author</label>
            <select
              className={`${errors?.userId?.message ? "is-invalid" : ""} form-select`}
              {...register("userId")}
            >
              {listUser?.map((user) => (
                <option key={user?.id} value={user?.id}>
                  {user?.name}
                </option>
              ))}
            </select>
            <span className="invalid-feedback">{errors?.userId?.message}</span>
          </div>
          <div className="mb-3">
            <label className="form-label">Content</label>
            <textarea
              className={`${errors?.body?.message ? "is-invalid" : ""} form-control`}
              {...register("body")}
            />
            <span className="invalid-feedback">{errors?.body?.message || ""}</span>
          </div>
          <button type="submit" className="btn btn-primary w-100 mt-3">
            Add Post
          </button>
        </form>
      </div>
    </MainLayout>
  );
}

export default NewPost;
