import { useSelector } from "react-redux";
import MainLayout from "../layouts/MainLayout";
import { listPostWithAuthorSelector } from "../store/selector";
import { Link } from "react-router-dom";
import paths from "../routes";

function HomePage() {
  const posts = useSelector(listPostWithAuthorSelector);
  return (
    <MainLayout>
      <div className="container">
        {posts.map((post) => (
          <div key={post.id} className="card w-100 my-5">
            <div className="card-body">
              <h5 className="card-title">{post.title}</h5>
              <p className="card-text">{post.body}</p>
              <Link
                to={paths.editPost.replace(":id", post.id)}
                className="card-link"
              >
                View Post
              </Link>
              <span> by {post.author?.name}</span>
              <i> 2 minutes ago</i>
              <div>Emoji here</div>
            </div>
          </div>
        ))}
      </div>
    </MainLayout>
  );
}

export default HomePage;
