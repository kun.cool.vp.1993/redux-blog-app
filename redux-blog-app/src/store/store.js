import { configureStore } from "@reduxjs/toolkit";
import postsSlice from "../redux-toolkit/postSlice";
import usersSlice from "../redux-toolkit/userSlice";

const store  = configureStore({
    reducer: {
        posts: postsSlice.reducer, 
        users: usersSlice.reducer
    }
});
export default store