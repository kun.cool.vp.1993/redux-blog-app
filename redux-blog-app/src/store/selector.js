import { createSelector } from "@reduxjs/toolkit"

export const listPostSelector = state=>state.posts.list
export const listUserSelector = state=>state.users.list
export const postSelector = state=>state.posts.post

export const listPostWithAuthorSelector = createSelector(
    listPostSelector,
    listUserSelector,
    (posts, users) =>{
        return posts.map(post=> ({
            ...post,
            author: (users.filter(user=> user.id === post.userId))[0]
        }))
    }
)

