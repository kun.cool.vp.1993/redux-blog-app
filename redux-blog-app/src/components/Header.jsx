import { Link } from "react-router-dom";
import paths from "../routes";


function Header() {
  return (
    <div className="w-100 bg-secondary">
      <div className="d-flex justify-content-between container">
        <h1 className="h1 text-white py-3">Redux Blog</h1>
        <ul className="nav">
          <li className="nav-item">
            <Link className="nav-link text-decoration-none text-light" to={paths.home}>
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-decoration-none text-light" to={paths.newPost} >
              New Post
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Header;
