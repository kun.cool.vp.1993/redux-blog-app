const paths = {
    home: '/',
    newPost: '/new-post',
    editPost: '/edit-post/:id',
}
export default paths