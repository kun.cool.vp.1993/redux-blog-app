import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const postsSlice = createSlice({
  name: "posts",
  initialState: {
    list: [],
    post: {
      id: "",
      userId: "",
      title: "",
      body: "",
      author: {
        id: "",
        name: "",
      },
    },
  },
  reducers: {
    updatePost: (state, action) => {
      state.post = action.payload;
    },
  },
  extraReducers: (builder) =>
    builder
      .addCase(fetchPostsThunkAction.fulfilled, (state, action) => {
        state.list = action.payload;
      })
      .addCase(fetchPostByIdThunkAction.fulfilled, (state, action) => {
        state.post = action.payload;
      })
      .addCase(UpdatePostThunkAction.fulfilled, (state, action) => {
        state.post = action.payload;
      })
      .addCase(DeletePostThunkAction.fulfilled, (state, action) => {
        state.post = action.payload;
      })
      .addCase(AddPostThunkAction.fulfilled, (state, action) => {
        state.post = action.payload;
      }),
});

export const fetchPostsThunkAction = createAsyncThunk(
  "fetchPostsThunkAction",
  async () => {
    let postListRes = await fetch(`https://jsonplaceholder.typicode.com/posts`);
    let data = await postListRes.json();
    return data;
  }
);
export const fetchPostByIdThunkAction = createAsyncThunk(
  "fetchPostByIdThunkAction",
  async (id) => {
    let postListRes = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
    let data = await postListRes.json();
    return data;
  }
);

export const UpdatePostThunkAction = createAsyncThunk(
  "UpdatePostThunkAction",
  async (data) => {
    let postRes = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${data?.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }
    );
    let res = await postRes.json();
    console.log(res);
    return res;
  }
);
export const AddPostThunkAction = createAsyncThunk(
  "AddPostThunkAction",
  async (data) => {
    let postRes = await fetch(`https://jsonplaceholder.typicode.com/posts`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    let res = await postRes.json();
    console.log(res);
    return res;
  }
);
export const DeletePostThunkAction = createAsyncThunk(
  "DeletePostThunkAction",
  async (id) => {
    let postRes = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    let res = await postRes.json();
    console.log(res);
    return res;
  }
);

export default postsSlice;
