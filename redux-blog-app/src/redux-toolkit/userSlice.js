import { createSlice ,createAsyncThunk } from "@reduxjs/toolkit";

const usersSlice = createSlice({
    name: "users",
    initialState: {
      list: [],
    },
    reducers: {},
    extraReducers: (builder) => builder
      .addCase(fetchUserThunkAction.fulfilled, (state,action) => {
          state.list = action.payload
      })    
  });

export const fetchUserThunkAction = createAsyncThunk('fetchUserThunkAction', async () => {
    let userListRes = await fetch(`https://jsonplaceholder.typicode.com/users`)
    let data = await userListRes.json();
    return data.map(item=>{
        return {
            id: item.id,
            name: item.name,
    }})
})
export default usersSlice