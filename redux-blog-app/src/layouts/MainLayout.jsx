import Header from "../components/Header"
import PropTypes from "prop-types";

function MainLayout({children}) {
  return (
    <div className="w-100">
        <Header />
        {children}
    </div>
  )
}

MainLayout.propTypes = {
    children: PropTypes.node.isRequired
}

export default MainLayout