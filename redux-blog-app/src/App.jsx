import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import paths from "./routes";
import NewPost from "./pages/NewPost";
import HomePage from "./pages/HomePage";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { fetchPostsThunkAction } from "./redux-toolkit/postSlice";
import { fetchUserThunkAction } from "./redux-toolkit/userSlice";
import EditPost from "./pages/EditPost";

function App() {
  const dispatch =  useDispatch();
  useEffect(()=>{
    dispatch(fetchPostsThunkAction());
    dispatch(fetchUserThunkAction());
  } 
  ,[dispatch])
  return (
    <Router>
      <div className="w-100">
        <Routes>
          <Route path={paths.newPost} element={<NewPost />} />
          <Route path={paths.home} element={<HomePage />} />
          <Route path={paths.editPost} element={<EditPost />} />
          <Route path={'/*'} element={<HomePage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
